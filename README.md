<img align="center" alt="logo DIO" height="60" width="150" src="https://user-images.githubusercontent.com/103616540/181390451-22cbef17-f3b6-4086-b8b9-e98281f080cd.png"/>

# Repositório de Desafios de Projeto



## Links Úteis

[Site da Dio](https://dio.me/)

[Sintaxe Básica Markdown](https://www.markdownguide.org/basic-syntax/)

[Download do git](https://git-scm.com/downloads)

[Download Ubuntu](https://ubuntu.com/download/desktop)

[GitLab](https://gitlab.com/)



## Tecnologias utilizadas nos projetos

<div style="display: inline_block">
  <img align="center" alt="git" height="70" width="60" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original-wordmark.svg"/>
  <img align="center" alt="GitHub" height="70" width="60" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/github/github-original-wordmark.svg" />
  <img align="center" alt="Ubuntu" height="70" width="60" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/ubuntu/ubuntu-plain-wordmark.svg" />
  <img align="center" alt="Docker" height="70" width="60" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/docker/docker-original-wordmark.svg" />
  <img align="center" alt="Kubernetes" height="70" width="60"  src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/kubernetes/kubernetes-plain-wordmark.svg" />
<img align="center" alt="Docker" height="70" width="60" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original-wordmark.svg" />

​              


​       

## Descrição do Desafio
Neste projeto será criado um pipeline de deploy de uma aplicação com dois cenários:

1. Deploy de uma aplicação Node.js em um container Docker.
2. Deploy da aplicação Node.js em cluster Kubernetes em nuvem utilizando o GCP(Google Cloud Platform).

Este projeto deverá ser apresentado em duas branchs. Uma das branchs com um deploy da aplicação sendo executada em um container e outra branch com a aplicação sendo executada em um cluster Kubernetes utilizando o GCP (Google Cloud Platform).



👍 A persistência é o caminho do êxito. 👍

